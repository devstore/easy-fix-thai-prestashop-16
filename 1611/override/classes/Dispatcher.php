<?php

/**
  *
  * EASY FIX THAI for PrestaShop 1.6.1.1
  *
  * @author devstore / devstore.in.th <devstore@hotmail.com>
  * @copyright devstore / devstore.in.th
  *
  */

class Dispatcher extends DispatcherCore
{
	public function __construct()
	{	 
		foreach ($this->default_routes as &$routes)
			foreach ($routes['keywords'] as &$keywords)
				$keywords['regexp'] = str_replace('\\pL', '\\pL\\pM', $keywords['regexp']);
		parent::__construct();
	}
}
