<?php
/*
*
* EASY FIX THAI for PrestaShop 1.6.0.14
*
* @author devstore / devstore.in.th <devstore@hotmail.com>
* @copyright devstore / devstore.in.th
*
*/
class HTMLTemplateInvoice extends HTMLTemplateInvoiceCore
{
	public function getContent()
	{
		$invoice_address = new Address((int)$this->order->id_address_invoice);
		$country = new Country((int)$invoice_address->id_country);

		$formatted_invoice_address = AddressFormat::generateAddress($invoice_address, array(), '<br />', ' ');
		$formatted_delivery_address = '';

		if ($this->order->id_address_delivery != $this->order->id_address_invoice)
		{
			$delivery_address = new Address((int)$this->order->id_address_delivery);
			$formatted_delivery_address = AddressFormat::generateAddress($delivery_address, array(), '<br />', ' ');
		}

		$customer = new Customer((int)$this->order->id_customer);

		$order_details = $this->order_invoice->getProducts();
		if (Configuration::get('PS_PDF_IMG_INVOICE'))
			foreach ($order_details as &$order_detail)
			{
				if ($order_detail['image'] != null)
				{
					$name = 'product_mini_'.(int)$order_detail['product_id'].(isset($order_detail['product_attribute_id']) ? '_'.(int)$order_detail['product_attribute_id'] : '').'.jpg';
					$path = _PS_PROD_IMG_DIR_.$order_detail['image']->getExistingImgPath().'.jpg';
					$order_detail['image_tag'] = preg_replace(
						'/\.*'.preg_quote(__PS_BASE_URI__, '/').'/',
						_PS_ROOT_DIR_.DIRECTORY_SEPARATOR,
						ImageManager::thumbnail($path, $name, 45, 'jpg', false),
						1
					);					
					if (file_exists(_PS_TMP_IMG_DIR_.$name))
						$order_detail['image_size'] = getimagesize(_PS_TMP_IMG_DIR_.$name);
					else
						$order_detail['image_size'] = false;
				}
			}

		$data = array(
			'order' => $this->order,
			'order_details' => $order_details,
			'cart_rules' => $this->order->getCartRules($this->order_invoice->id),
			'delivery_address' => $formatted_delivery_address,
			'invoice_address' => $formatted_invoice_address,
			'tax_excluded_display' => Group::getPriceDisplayMethod($customer->id_default_group),
			'tax_tab' => $this->getTaxTabContent(),
			'customer' => $customer
		);

		if (Tools::getValue('debug'))
			die(json_encode($data));

		$this->smarty->assign($data);

		return $this->smarty->fetch($this->getTemplateByCountry($country->iso_code));
	}
}

