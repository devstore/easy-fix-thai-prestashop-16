<?php

/**
  *
  * EASY FIX THAI for PrestaShop 1.6.0.14
  *
  * @author devstore / devstore.in.th <devstore@hotmail.com>
  * @copyright devstore / devstore.in.th
  *
  */

class Tools extends ToolsCore
{
	public static function str2url($str)
	{
		static $allow_accented_chars = null;

		if ($allow_accented_chars === null)
			$allow_accented_chars = Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL');

		if (!is_string($str))
			return false;
		$str = trim($str);

		if (function_exists('mb_strtolower'))
			$str = mb_strtolower($str, 'utf-8');
		if (!$allow_accented_chars)
			$str = Tools::replaceAccentedChars($str);

		// Remove all non-whitelist chars.
		if ($allow_accented_chars)
			$str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-\pL\pM]/u', '', $str);	
		else
			$str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-]/','', $str);
		
		$str = preg_replace('/[\s\'\:\/\[\]\-]+/', ' ', $str);
		$str = str_replace(array(' ', '/'), '-', $str);

		// If it was not possible to lowercase the string with mb_strtolower, we do it after the transformations.
		// This way we lose fewer special chars.
		if (!function_exists('mb_strtolower'))
			$str = Tools::strtolower($str);

		return $str;
	}
}

