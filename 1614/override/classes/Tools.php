<?php

/**
  *
  * EASY FIX THAI for PrestaShop 1.6.1.4
  *
  * @author devstore / devstore.in.th <devstore@hotmail.com>
  * @copyright devstore / devstore.in.th
  *
  */

class Tools extends ToolsCore
{
	public static function str2url($str)
	{
		static $array_str = array();
		static $allow_accented_chars = null;
		static $has_mb_strtolower = null;

        if ($has_mb_strtolower === null) {
			$has_mb_strtolower = function_exists('mb_strtolower');
        }

        if (isset($array_str[$str])) {
			return $array_str[$str];
        }

        if (!is_string($str)) {
			return false;
        }

        if ($str == '') {
			return '';
        }

        if ($allow_accented_chars === null) {
			$allow_accented_chars = Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL');
        }

		$return_str = trim($str);

        if ($has_mb_strtolower) {
			$return_str = mb_strtolower($return_str, 'utf-8');
        }
        if (!$allow_accented_chars) {
			$return_str = Tools::replaceAccentedChars($return_str);
        }

		// Remove all non-whitelist chars.
        if ($allow_accented_chars) {
            $return_str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-\p{L}\pM]/u', '', $return_str);
        } else {
			$return_str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-]/','', $return_str);
        }

		$return_str = preg_replace('/[\s\'\:\/\[\]\-]+/', ' ', $return_str);
		$return_str = str_replace(array(' ', '/'), '-', $return_str);

		// If it was not possible to lowercase the string with mb_strtolower, we do it after the transformations.
		// This way we lose fewer special chars.
        if (!$has_mb_strtolower) {
			$return_str = Tools::strtolower($return_str);
        }

		$array_str[$str] = $return_str;
		return $return_str;
	}
}

