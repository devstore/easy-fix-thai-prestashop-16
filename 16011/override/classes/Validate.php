<?php

/**
  *
  * EASY FIX THAI for PrestaShop 1.6.0.11
  *
  * @author devstore / devstore.in.th <devstore@hotmail.com>
  * @copyright devstore / devstore.in.th
  *
  */

class Validate extends ValidateCore
{
	public static function isLinkRewrite($link)
	{
		if (Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL'))
			return preg_match('/^[_a-zA-Z0-9\-\pL\pM]+$/u', $link);
		return preg_match('/^[_a-zA-Z0-9\-]+$/', $link);
	}

	public static function isRoutePattern($pattern)
	{
		if (Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL'))
			return preg_match('/^[_a-zA-Z0-9\(\)\.{}:\/\-\pL\pM]+$/u', $pattern);
		return preg_match('/^[_a-zA-Z0-9\(\)\.{}:\/\-]+$/', $pattern);
	}
}