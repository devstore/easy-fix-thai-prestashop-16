<?php
/**
  *
  * EASY FIX THAI for PrestaShop 1.6.1.0
  *
  * @author devstore / devstore.in.th <devstore@hotmail.com>
  * @copyright devstore / devstore.in.th
  *
  */
abstract class Controller extends ControllerCore
{
    /*** All other previous overrides here ***/

    /**
     * Use Google's CDN to host jQuery
     * @param string  $version  Version of jQuery to include
     * @param string  $folder   Not used in this override
     * @param boolean $minifier Use minified version?
     */
    public function addJquery($version = null, $folder = null, $minifier = true) {

        $this->addJS(Media::getJSPath(Tools::getCurrentUrlProtocolPrefix() . 'ajax.googleapis.com/ajax/libs/jquery/' . ($version ? $version : _PS_JQUERY_VERSION_) . '/jquery'.($minifier ? '.min.js' : '.js')));

    }
}
