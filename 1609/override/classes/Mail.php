<?php

/**
  *
  * EASY FIX THAI for PrestaShop 1.6.0.9
  *
  * @author devstore / devstore.in.th <devstore@hotmail.com>
  * @copyright devstore / devstore.in.th
  *
  */

class Mail extends MailCore
{	
	public static function isMultibyte($data)
	{
		$length = mb_strlen($data);

		for ($i = 0; $i < $length; $i++)
		{
			$result = ord(($data[$i]));

			if ($result > 128)
			{
				return true;
			}
		}

		return false;
	}

	public static function mimeEncode($string, $charset = 'UTF-8', $newline = "\r\n")
	{
		if (!self::isMultibyte($string) && mb_strlen($string) < 75)
		{
			return $string;
		}

		$charset = strtoupper($charset);
		$start   = '=?' . $charset . '?B?';
		$end     = '?=';
		$sep     = $end . $newline . ' ' . $start;
		$length  = 75 - mb_strlen($start) - mb_strlen($end);
		$length  = $length - ($length % 4);

		if ($charset === 'UTF-8')
		{
			$parts = array();
			$maxchars = floor(($length * 3) / 4);
			$stringLength = mb_strlen($string);

			while ($stringLength > $maxchars)
			{
				$i = (int)$maxchars;
				$result = ord($string[$i]);

				while ($result >= 128 && $result <= 191)
				{
					$i--;
					$result = ord($string[$i]);
				}

				$parts[] = base64_encode(mb_substr($string, 0, $i));
				$string = mb_substr($string, $i);
				$stringLength = mb_strlen($string);
			}

			$parts[] = base64_encode($string);
			$string = implode($sep, $parts);
		}
		else
		{
			$string = chunk_split(base64_encode($string), $length, $sep);
			$string = preg_replace('/' . preg_quote($sep) . '$/', '', $string);
		}

		return $start . $string . $end;
	}
}
